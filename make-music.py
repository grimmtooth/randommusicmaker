#
# Let's define some things.
#
cd_size         = 700000000 # size of a CD in bytes
thumb_dir_limit = 100       # max number of songs per thumbdrive dir
media_root      = "D:\Music\Mai Music\iTunes\iTunes Media"
destination     = "D:\\t\\to_burn"

import argparse
import os
import os.path
import random
import shutil
import sys

collected_music_list = []

def collect_music(x, dir_name, files):
    print "Searching [%s] ..." % dir_name

    for f in files:
        if f.lower().endswith('mp3'):
            print "---> Adding [%s]" % f
            collected_music_list.append("%s\%s" %(dir_name, f))


def createStick():
    print "Not yet implemented. Try again another day."
    sys.exit(1)


def createCDs():
        def new_folder(number):
            dest_dir = "%s\%03d" % (destination, number)
            print "Creating new folder [%s]" % dest_dir
            os.makedirs(dest_dir)
            return dest_dir

        print "I will create a series of CDs. Have media ready."
        os.path.walk(media_root, collect_music, 0)
        print "Collection complete. Randomizing [%d] songs." % len(collected_music_list)
        random.shuffle(collected_music_list)
        print collected_music_list[0]
        print "done"

        accumulated_size = 0
        current_folder = 0
        current_file_number = 0
        destination_dir = new_folder(current_folder)

        for song in collected_music_list:
            file_size = os.path.getsize(song)
            accumulated_size = accumulated_size + file_size

            if accumulated_size > cd_size:
                print "Disk full, creating new disk."
                #accumulated_size = accumulated_size - file_size
                current_folder = current_folder + 1
                destination_dir = new_folder(current_folder)
                accumulated_size = file_size
                current_file_number = 0

            dest_file = "%s\%04d.mp3" % (destination_dir, current_file_number)
            print "   Copying %s ---> %s" % (song, dest_file)
            shutil.copyfile(song, dest_file)

            current_file_number = current_file_number + 1



        print "Done!"
        sys.exit(0)


def main(args):
    toStick = args.toStick

    if toStick:
        createStick()
    else:
        createCDs()


class myArgParser(argparse.ArgumentParser):
    description = """
Creates a random, but static, playlist from the contents of a media directory that contains MP3 files. The
files will be renamed with a four-digit numeric prefix so they will be written in a specific order and appear
to any observer in that order.
    """

    toStickHelp = """
Flag to determine whether I am writing to a CD or a thumbdrive. If going to a CD, I will burn
%d bytes of data to it then ask for a new CD.  If a thumbdrive, then I will create directories
having maximum of %d songs in each on the same drive.  If you leave this argument off, I will create a CD.
If you specify -t, I will create a thumbdrive.
    """ % (cd_size, thumb_dir_limit)

    def __init__(self, *args, **kwargs):
        kwargs['description'] = self.description
        argparse.ArgumentParser.__init__(self, *args, **kwargs)

        self.add_argument('-t', action='store_true', required=False, dest='toStick', help=self.toStickHelp)

    def error(self, message):
        print "Error: %s" % message
        self.print_help()
        sys.exit(2)


if __name__ == "__main__":
    class dummyArgs:
        toStick = False

    parser = myArgParser()
    #args = dummyArgs()
    args = parser.parse_args()
    main(args)
